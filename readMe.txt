Site da empresa júnior Humaniza Consultoria Júnior em Gestão de Pessoas na sua versão final com o Back-end implementado.

Imagens do projeto: https://drive.google.com/drive/folders/1z_G7QwCG3b_dw9U-r3xcWO7nkkZ5lU0p?usp=drive_link

As tecnologias usadas para desenvolver esse projeto foram: Html, Css, Javascript, Json, Fetch, DOM, Sass, Bootstrap, Nodejs, Nodemailer.

Conhecimentos adquiridos: Durante o desenvolvimento obtive o conhecimento em como implementar um servidor web nodejs capaz de 
renderizar minha pagina html (arquivo .html), como criar uma rota e qual sua logica por trás, a manusear e consumir a API
nodemailer para enviar email formatado, a como enviar dados do fetch para minha rota no servidor web nodejs da minha pagina 
html (usando formulario com input's e textarea) e também para finalizar como é a estrutura de diretorios para um servidor web.
Como complemento do css no bootstrap houve se o aprendizado do uso da tecnologia/ferramenta Sass.

Desafios que tive: Obtive dificuldades em como arquitetar os arquivos do back-end (do servidor web), em como usar o nodemailer
por causa que o gmail da google mudou suas diretrizes e alguns conteudos na internet ficaram defasados dificultando a manipulação
da API, alguns conceitos de ouvir portas foi encontrado devido que programas instalados ocupavam minhas portas.

Informações adicionais ao site humaniza completo com back-end integrado:
1) Para que o nodemailer funcione hoje em dia determinaram que deve se criar uma "senha de aplicativo" e para poder criar
isso é necessario ativar em SEGURANÇA da conta contato@humanizajr.com.br a "verificação em duas etapas" após criado essa
senha vá até a pasta webserver e procure o arquivo server.js e dentro da variavel const pass (linha 12) atribua essa senha
nova gerada;
2) O servidor web nodejs que eu criei (não usei express) dentro da pasta webserver possui um arquivo index.js sua 
responsabilidade é ser um cabeçalho e consta o LISTEN do servidor web. Para conseguirmos linkar o dominio humanizajr.com.br
no nosso servidor web devemos hospedar a pasta webserver no servidor e pegar o endereço ipv4 da maquina (hospedagem escolhe
randomicamente que maquina será essa e lhe dá acesso) juntamente com a porta (7070 determinada por default no codigo) e
no site registrobr (nosso site de dominio) achar nas configurações aonde jogar esse endereço composto pelo ipv4:porta deixando
assim nosso dominio ouvindo a hospedagem. Por fim pegamos o endereço que no fim será gerado para nós no passo anterior
pelo registrobr e colocar como segundo argumento do metodo LISTEN (webServer.listen) do arquivo index.js (linha 10).
Observação: Essa atividade de fazer no registrobr possui nome que é "criar um subdominio" a partir de um dominio existente;
3) O pm2 não instalei no meu servidor web mas é necessario porque ao rodar no servidor precisamos usar ele siga as etapas abaixo:
I - Estando no diretorio webserver execute no terminal o comando "npm install pm2 -g";
II - Execute no terminal o comando "pm2 start index.js";
Assim o meu aplicativo (site humaniza) será iniciado e gerenciado com o PM2. O pm2 mantém o processo em execução mesmo se o
servidor for reiniciado ou se o terminal for fechado. Alguns recursos interessantes como o monitoramente em tempo real, logs
detalhados e recuperação automático em caso que sua hospedagem perca energia e o computado reinicie. 
O pm2 consegue trabalhar com o nodejs normalmente e recomenda se ao iniciar o servidor na hospedagem dar um nome a aplicação
para ficar facil visualizar ele funcionando no sistema (PID) e manusear os recursos interessantes para isso execute o comando
pm2 start index.js --name "SiteHumaniza" dentro das aspas pense em um nome criativo rs;
Observação: devo procurar estudar o comando "pm2 scale" na definição alega que minha aplicação lide com cargas de trabalho
crescentes escalando ele dinamicamente;
