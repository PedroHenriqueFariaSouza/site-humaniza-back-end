

document.addEventListener("visibilitychange", function () {
    console.log("Mudou de aba!")
});


const button = document.getElementById("botaoEnviar");


button.addEventListener("click", function () {
    const nome = document.getElementById("nomeInput").value;
    const email = document.getElementById("emailInput").value;
    const telefone = document.getElementById("telefoneInput").value;
    const descricao = document.getElementById("descricaoTextarea").value;
  fetch("/enviarEmail" , {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({
      nome:nome,
      email:email,
      telefone:telefone,
      descricao:descricao})
  })
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(error => console.log(error));
});


