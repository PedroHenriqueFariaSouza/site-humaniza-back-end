var fs = require('fs'); //uso metodo de arquivamento comum de txt para manipular arquivos nada de expecional aqui.

module.exports = function (filename, successFn, errorFn) {
    fs.readFile(filename, function (err, data) { //exportei essa função para server.js e la fullpath passa para aqui substituindo filename.
        if (err) {
            errorFn(err);
        } else {
            successFn(data);
        }
    });
};