process.title = 'BackEndSiteHumaniza';
var args = process.argv,
    port = args[2] || 7070,
    webServer = require('./server'); //poderia jogar todo conteudo de server.js aqui que funcionaria mais quis dividir tudo
    //em modulos por isso o require ta sendo usado puxa todo corpo de server.js aqui mas ele está separado em outro arquivo.

    const sendEmail = require("./server").sendEmail; //precisei importar essa porra aqui pro script.js entender
    //que aquela função é daqui

webServer.listen(port, function(){
    console.log("Servidor esta funcionando!!!");
    console.log("Porta usada e : "+ port);
}); 


//inicializar o server e tudo após aqui não será executado. O primeiro argumento é a porta que ele vai ouvir e
// a function apenas imprimir no console log do navegador que está tudo funcionando.

/* Step 1
Para que o servidor funcione necessitamos criar uma sequência de pastas em uma ordem que o NODE compreende sendo elas:
1) criar uma pasta de nome webserver a qual vai receber dentro dela as demais pastas (em outras palavras aonde residirá
a nossa aplicação);
2) criar uma pasta de nome static onde ficarão os arquivos providos pelo servidor;
3) criar uma pasta css a qual conterá os arquivos de estilo e o saas.
4) criar uma pasta image para hospedar todas imagens que o html conterá;
5) e por fim uma pasta script para se houver necessidade em manipular o DOM e outros scripts no html;
Teremos agora que criar os arquivos responsaveis pelo servidor os quais devem ficar fora da pasta static mas dentro
da pasta webserver: 
1) index.js que representa o arquivo principal da aplicação;
2) server.js que é o servidor em si mesmo;
3) filehandler.js que carregará os arquivos solicitados pelo browser usando arquivamento;
4) config.json este json é responsavel por conter as configurações do servidor como por exemplo diretorio padrão a ser
escutado (static que node usa por default) , se o html possuir imagem do tipo jpeg deve ser declarado aqui também
e demais arquivos que forem necessarios para compor nosso html/css. Vale ressaltar que nesse arquivo quando o usuario não
define qual pagina raiz (qnd so digita o site www.algo.com.br) e o barra (/) não existe indicando uma rota um html
padrão será definido.

Recomenda se usar o NODEMON do NPM para evitar transtornos de toda vez que fazer uma modificação não precisar
ir ao terminal e digitar node nomeDoArquivo.js a cada letra que você mudar do codigo ja que o nodemon detecta alterações
e reinicia nosso servidor sozinho.

Dentro da pasta webserver execute comando npm init ao gerar o arquivo package.json vá até ele e se coloque o nome como
proprietario dessa aplicação ela poderá conter outros dados como depedências necessarias, licenças e etc;
Instale o nodemon com o comando no terminal 'npm install nodemon';

index.js = responsavel por fazer a inclusão e utilização de tdos os módulos da aplicação. Se o servidor web pode fazer
algo ou alguma coisa aqui ele será descrito. Esse arquivo é como se fosse um container centralizando a utilização de 
modulos, que serão separados por responsabilidades (cada modulo é responsavel por uma parte do trabalho).
Na linha um temos process.title responsavel por nomear a aplicação que será um processo no sistema operacional quando
abrimos o gerenciador de tarefa e vêmos o programas em execução o nome definido aqui existirá também lá;
A linha que contem var args = process.argv e sua linha sucessora podemos caso queiramos removê las porque ao digitarmos
nodemon index.js NumeroDaPorta definimos um valor numerico no teclado e caso seja omitido esse valor numerico por default
será designado a porta 7070 contido no codigo da linha sucessora;
O resto desse codigo consta como uma invocação do webserver existente no nucleo do NODEJS e a abertura do LISTEN que é a
porta que ficará ouvindo nossa aplicação caso dê tudo certo o console log executará;

Observação: tudo que estiver de codigo após o LISTEN não será executado.
Observação: por padrão o node entende que a invocação do require server aqui presente necessita de um arquivo de nome
server.js como recriamos ele tudo nele contido será a implementação do servidor web com as requisições http.
*/