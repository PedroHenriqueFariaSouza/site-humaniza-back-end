var http = require('http'),
    config = require('./config'),
    fileHandler = require('./filehandler'),
    parse = require('url').parse,
    types = config.types,
    rootFolder = config.rootFolder,
    defaultIndex = config.defaultIndex,
    server;

const nodemailer = require("nodemailer");
const userAdress = "contato@humanizajr.com.br";
const pass = "reiuhglwonxkrhws"; //precisa verificar qual senha desse email se ñ foi alterada nem nada.


module.exports = server = http.createServer();
//servidor Online e funcionando aguardando requisições (requests) o segundo parametro 
// é uma função de callback para executar algumas instruções necessarias.

server.on('request', onRequest);

function onRequest(req, res) {
    var filename = parse(req.url).pathname,
        fullPath,
        extension;

    if (filename === '/') { //se o usuario ñ definiu qual arquivo (html) queria ao requisitar servidor nos carrega um
        filename = defaultIndex;  //arquivo generico para usuario n receber tela branca e aqui armazenamos nessa variavel.
    } //aonde especifiquei que defaultIndex é meu html arquivo generico? dentro do config.json

    else if (filename === '/enviarEmail' && req.method === 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString();
        });
        req.on('end', async () => {
            const {nome, email, telefone, descricao} = JSON.parse(body);
            console.log("servidor recebeu os dados nome :"+nome+" , email: "+email+" , telefone: "+telefone+" , descrição: "+descricao);
             //Inicio do nodemailer
            try {
                let transporter = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 587,
                    secure: false,
                    auth: {
                        user: userAdress,  
                        pass: pass  
                    },
                    tls: {rejectUnauthorized:false}
                });
                
                let mailOptions = {
                    from: userAdress, 
                    to: email,
                    subject: 'Mensagem do contato Site Humaniza.',
                    text: "Nome: " + nome + "\n" + "Email: "+ email + "\n" + "Telefone: "+ telefone + "\n" + "Descrição: " + descricao + "\n" + "Por favor colaborador da humaniza responda o cliente em tempo habil esse robô da humaniza fica feliz com sua cooperação."
                };
                
                let info = await transporter.sendMail(mailOptions);
                console.log("Email enviado:" +info.messageId);
            } catch (error) {
                console.error(error);
            }
            //fim do nodemailer
            res.end();
        });
    }

    fullPath = rootFolder + filename; //criei apenas o diretorio aonde o arquivo está e concatenei com o html solicitado (ex: c:/weberver/statis/index);
    extension = filename.substr(filename.lastIndexOf('.') + 1); // acima flei do diretorio aonde aquivo está e o seu nome agr aqui falamos se é html, jpeg, png, ...

    //filehandler tem 3 argumetos o primeiro é o diretorio completo aonde meu html está para ser servido ao cliente,
    //uma função de callback se tudo der certo e outra função de callback para caso aconteça um erro.

    fileHandler(fullPath, function (data) { //função responsavel por ler o arquivo que for pedido dentro da pasta static
        res.writeHead(200, { //json do callback de sucesso  
            'Content-Type': types[extension] || 'text/plain', //contendo tipo do arquivo devolvido(content type)
            'Content-Length': data.length    //e o tamanho desse dado que será enviado na camada fisica
        });
        res.end(data);//encerro a comunicação com o cliente se for escolhida a função de callback de sucesso e envio 
        //todos os dados para o cliente que fez a requisição (o data dentro da função end).

    }, function (err) {
        res.writeHead(404); //se callback de erro foi escolhido apenas mando o codigo sem nenhum json desenvolvido (n quis criar)
        res.end(); //porque usei o end se n tem os dados (data) dentro do parameto do end()? oras porque deu erro usar apenas
    });              //para encerrar a conexão com o cliente.

    //dentro da função de callback de erro poderia alongar fazendo os outros codigos de erro do http como 304 e 500 ñ fiz por preguiça.

}

/* Step 2
No inicio desse documento fizemos alguns requires de arquivos que criamos anteriormente;
O require do metodo parse() é importante porque na URL temos rotas (ou caminhos) como por exemplo 
www.google.com.br/admin/usuario nesse exemplo o parte URL vai separar cada trecho da URL para tratarmos eles como
objetos individuais (objeto admin direcionando um html certo e usuario com outro html);
No arquivo filehandler fizemos um script para ser exportado ao uso de outros arquivos caso seja necessario (e será). Neste
arquivo temos uma função de callback podendo nos disponibilizar uma mensagem de erro caso o arquivo html não exista e sim
atualmente sua unica função é usar arquivamento pra pegar o html (não um txt como é trivial);
No metodo onRequest tem dois argumentos o req é acionado quando uma requisição de algum lugar é feita ja o res é a resposta
para o cliente pedindo a requisição podendo ser os codigos do protocolo HTTP (erro ou sucesso) e o proprio conteudo da
resposta (o html, css e as imagens);
A variavel REQ no onRequest é um objeto que possui informações que o servidor precisa ter conhecimento como por exemplo
qual navegador ta sendo usado pelo cliente, IP, qual arquivo ta sendo solicitado;
O parse apenas faz a interpretação do endereço http transformando em um json sendo assim podemos trabalhar com os valores
da URL (que são separados por partes) do tipo pathname, hostname, port e search (tem outros além desses);
Config.json tem declarado TODOS tipos de arquivos existentes no meu servidor web caso existisse um PDF la eu deveria ir
até esse arquivo e colocar la declarado o pdf (que não tem porque ñ existe pdf);

*/